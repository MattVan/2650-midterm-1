// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { data } = context;
    //to see whats happening
    console.log( data );
    
    if(data.year.parseInt() < 1885 || data.year.parseInt() >  2020){
      throw new Error("Car cannot be newer than 2020 or older than 1885");
    }
    
    if(data.mileage.parseInt() < 0){
      throw new Error("Mileage cannot be negative");
    }
    
    if(data.make.trim().length == 0 || data.model.trim().length == 0){
      throw new Error("Make or model cannot be empty");
    }
    
    if(data.make.length > 30){
      data.make = data.make.substring(0,31);
    }
    
    if(data.model.length > 30){
     data.model = data.model.substring(0,31);
    }
    
    return context;
  };
}
